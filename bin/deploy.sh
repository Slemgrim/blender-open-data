#!/usr/bin/env bash

HOST="root@46.101.192.91"

rsync -avzv --exclude 'node_modules' --exclude 'static/webpack_bundles' . $HOST:/root/opendata

