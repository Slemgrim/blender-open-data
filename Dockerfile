FROM nikolaik/python-nodejs:latest

ENV PYTHONUNBUFFERED 1
RUN pip3 install pipenv

RUN mkdir /code
WORKDIR /code
ADD . /code/
RUN pipenv install --deploy --system

RUN python manage.py collectstatic --noinput

RUN npm install --ignore-optional
RUN npm run build

VOLUME /code/assets
