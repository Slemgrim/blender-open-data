# Opendata

Public open data portal for benchmark results

## Deployment setup

1. copy `./opendata/_settings.py` to `./opendata/settings.py` and adjust to your needs
2. Add a `.env` file.
3. Start services with `docker-compose run`
4. Run `docker-compose exec web python manage.py migrate`
5. Create Superuser `docker-compose exec web python manage.py createsuperuser`
6. Open browser, login at `/admin` and create a new auth token.
7. Share auth token with client (mydata)

## Development setup

Opendata uses [pipenv](https://docs.pipenv.org/) for development. Make sure to install it first. 

After cloning the Git repo, perform these steps to create a working dev server:

1. copy `./.env-sample` to `./.env` and adjust to your needs
2. Install requirements with `pipenv install`
3. Start your virtaulenv with `pipenv shell` 
4. Setup database `docker-compose -f docker-compose-dev.yml up` or provide a database/redis/elasticsearch in `./opendata/settings.py`
5. Run migrations  `./manage.py migrate`
6. Install frontend dependencies `npm install`
7. Serve frontend  `npm run serve`
8. Run application `./manage.py runserver`
9. Run `./manage.py createsuperuser` to create an admin user
10. Open browser, login at `/admin` and create a new auth token.
11. Share auth token with client (mydata)

## Fixtures

1. Call `./manage.py loaddata ./examples/fixture.json` to import fixture data
2. Call `http://opendata.local:8002/benchmark-query/index/` to index all benchmarks. 
(This only works when you're logged in as superuser)


## Try it out

Look at `./examples/example.py` to see some basic queries.
Or open the example query inside your browser http://opendata.local/benchmarks/render-time-by-gpu/

You can also access the admin interface and add new queries. 
Queries are based on elasticsearchs [Query DSL](https://www.elastic.co/guide/en/elasticsearch/reference/6.3/query-dsl.html)
Queries need a slug to be easily accessible from the browser. 


## Endpoints

### Benchmark

Endpoint | Properties | Payload | Description 
--- | --- | --- | --- 
`POST http://apiopendate.local/api/benchmarks/` | - | a valid benchmark. See benchmark.json in examples folder | creates a new benchmark. You need to provide an auth token
`GET http://opendate.local/api/benchmarks/{benchmark_id}` | * `benchmark_id`: id of an existing benchmark | - | retrieves a benchmark
`DELETE http://opendate.local/api/benchmarks/{manage_id}` | * `manage_id: you get this id from a POST request | - | Deletes a benchmark.`Can only be used by the owner of a manage_id`

### Queries

Endpoint | Properties | Payload | Description 
--- | --- | --- | --- 
`GET http://opendate.local/api/benchmark-query/` | - | - | Displays all indexed benchmarks. This will be removed in the future
`GET http://opendate.local/api/benchmark-query/{query_slug}` | * `query_slug`: name of an existing query | - | retrieves benchmark data based on a predefined query
`GET http://opendate.local/api/benchmark-query/incex` | - | - | Indexes all benchmarks. Only as superuser

### Example cURL

```
curl -X POST \
  http://opendata.local:8002/api/benchmarks/ \
  -H 'Authorization: Token b4e40a77702d3fb563a17f213a19bdb60c7648a6' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  --data-binary "@path/to/benchmark"
```

### Running Tests

All tests are locatet inside the `tests` directory. 
Run `./manage.py test tests`