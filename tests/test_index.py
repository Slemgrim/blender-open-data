from django.test import TestCase


class IndexTest(TestCase):

    def test_dashboard(self):
        """
        Ensure dashboard has a route
        """
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
