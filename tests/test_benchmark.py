import json

from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase
from web.models import Benchmark


def add_token(func):
    def _add_token(self, *args, **kwargs):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        func(self, *args, **kwargs)

    return _add_token


class BenchmarkTests(APITestCase):
    token = None
    user = None
    benchmark = None
    url = '/api/benchmarks/'

    def setUp(self):
        self.user = User.objects.create_user(username='slemgrim')
        self.token = Token.objects.create(user=self.user)
        with open('./examples/benchmark.json') as f:
            self.benchmark = json.load(f)

    def test_post_benchmark_checks_token(self):
        """
        Ensure we can't create benchmarks without a token.
        """
        response = self.client.post(self.url, self.benchmark, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_post_checks_valid_token(self):
        """
        Ensure we can't create benchmarks without a valid token.
        """
        self.client.credentials(HTTP_AUTHORIZATION='Token invalid')
        response = self.client.post(self.url, self.benchmark, format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    @add_token
    def test_post_invalid_benchmark(self):
        """
        Ensure we can't create a benchmark with missing data.
        """
        self.benchmark['data']['blender_version'] = None
        response = self.client.post(self.url, self.benchmark, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @add_token
    def test_post_benchmark(self):
        """
        Ensure we can create a new benchmark.
        """
        version = "test-version-name"
        self.benchmark['data']['blender_version']['version'] = version
        response = self.client.post(self.url, self.benchmark, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Benchmark.objects.count(), 1)
        self.assertEqual(Benchmark.objects.get().data['blender_version']['version'], version)

    @add_token
    def test_get_benchmark(self):
        """
        Ensure we can retrieve benchmarks
        """
        response = self.client.post(self.url, self.benchmark, format='json')
        response = self.client.get(self.url + response.data['benchmark_id'] + '/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_missing_benchmarks(self):
        """
        Ensure we can't retrieve benchmarks that don't exist
        """
        response = self.client.get(self.url + '123-123/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_trailing_slash_redirect(self):
        """
        Ensure we can't retrieve benchmarks that don't exist
        """
        response = self.client.get(self.url + 'no-trailing-slash')
        self.assertEqual(response.status_code, status.HTTP_301_MOVED_PERMANENTLY)

    @add_token
    def test_delete_benchmark_without_manage_id(self):
        """
        Ensure we can't delete without knowing the manage_id
        """
        response = self.client.post(self.url, self.benchmark, format='json')
        response = self.client.delete(self.url + response.data['benchmark_id'] + '/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    @add_token
    def test_delete_benchmark(self):
        """
        Ensure we can delete with the manage_id
        """
        response = self.client.post(self.url, self.benchmark, format='json')
        response = self.client.delete(self.url + str(response.data['manage_id']) + '/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
