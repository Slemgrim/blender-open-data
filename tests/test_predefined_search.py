from rest_framework import status
from rest_framework.test import APITestCase

from web.models import PredefinedSearch


class PredefinedSearchTests(APITestCase):
    url = '/api/benchmark-query/'

    def test_string_representation(self):
        """
        Ensure queries are displayed with its name
        """
        search = PredefinedSearch(name="human readable title")
        self.assertEqual(str(search), search.name)

    def test_string_representation_without_name(self):
        """
        Ensure queries display fallback to the slug
        """
        search = PredefinedSearch(name="", slug="the-slug")
        self.assertEqual(str(search), search.slug)

    def test_access_query_by_slug(self):
        """
        Ensure queries can be accessed over http
        """
        slug = "the-slug"
        search = PredefinedSearch(slug=slug)
        search.save()
        response = self.client.get(self.url + slug + '/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_missing_slugs(self):
        """
        Ensure missing queries return 404
        """
        response = self.client.get(self.url + 'missing-query/')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
