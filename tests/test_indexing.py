import json
from time import sleep

import elasticsearch_dsl
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase
from web.search import BenchmarkIndex
from tests.test_benchmark import add_token


class BenchmarkIndexTests(APITestCase):
    token = None
    user = None
    benchmark = None
    url = '/api/benchmarks/'

    def delete_index(self):
        try:
            elasticsearch_dsl.Index('benchmark-testing').delete()
        except:
            None

    def setUp(self):
        self.delete_index()
        self.user = User.objects.create_user(username='slemgrim')
        self.token = Token.objects.create(user=self.user)
        with open('./examples/benchmark.json') as f:
            self.benchmark = json.load(f)

    @add_token
    def test_post_benchmark(self):
        """
        Ensure we can create a new benchmark.
        """
        version = "test-version-name"
        self.benchmark['data']['blender_version']['version'] = version
        response = self.client.post(self.url, self.benchmark, format='json')
        benchmark_id = response.data['benchmark_id']
        sleep(1)  # We need this to wait for the benchmark to be index by elasticsearch
        benchmarks, aggs, hits = BenchmarkIndex.query(
            {
                "query": {
                    "ids": {
                        "values": str(benchmark_id)
                    }
                }
            }
        )
        self.assertEqual(hits, 1)

    @add_token
    def test_delete_benchmark(self):
        """
          Ensure we can create a new benchmark.
        """
        version = "test-version-name"
        self.benchmark['data']['blender_version']['version'] = version
        response = self.client.post(self.url, self.benchmark, format='json')
        benchmark_id = response.data['benchmark_id']
        manage_id = response.data['manage_id']
        self.client.delete(self.url + str(manage_id) + '/')
        benchmarks, aggs, hits = BenchmarkIndex.query(
            {
                "query": {
                    "ids": {
                        "values": benchmark_id
                    }
                }
            }
        )

        self.assertEqual(hits, 0)
