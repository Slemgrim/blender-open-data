import uuid
from django.contrib.postgres.fields import JSONField
from django.db import models

from web.search import BenchmarkIndex


class Benchmark(models.Model):
    benchmark_id = models.UUIDField(default=uuid.uuid4, editable=False, db_index=True)
    manage_id = models.BinaryField(db_index=True)
    date_created = models.DateTimeField(auto_now_add=True)
    data = JSONField()

    class Meta:
        ordering = ('date_created',)

    def create_index(self):
        obj = BenchmarkIndex(
            meta={'id': self.benchmark_id},
            data=self.data,
            benchmark_id=self.benchmark_id,
            date_created=self.date_created,
        )
        obj.save()
        return obj.to_dict(include_meta=True)

    def remove_index(self):
        obj = BenchmarkIndex.get(self.benchmark_id)
        if obj is not None:
            obj.delete()

    def as_dict(self):
        return {
            "benchmark_id": self.benchmark_id,
            "date_created": self.date_created,
            "data": self.data
        }


class PredefinedSearch(models.Model):
    slug = models.SlugField(db_index=True, primary_key=True)
    name = models.CharField(blank=True, max_length=255)
    description = models.TextField(blank=True)
    query = JSONField(help_text="Elasticsearch Query", default={})
    date_created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('date_created',)

    @staticmethod
    def get_by_slug(slug):
        return PredefinedSearch.objects.get(slug=slug)

    def __str__(self):
        return self.name if self.name else self.slug
