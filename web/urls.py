from django.conf.urls import url

from web import api_views, views

urlpatterns = [
    url(r'^$', views.index, name='index'),

    url(r'^api/benchmarks/$', api_views.BenchmarkListApiViews.as_view()),
    url(r'^api/benchmarks.json$', api_views.download_benchmarks),
    url(r'^api/benchmarks/(?P<pk>[0-9a-z-]+)/$', api_views.BenchmarkDetailApiViews.as_view()),

    url(r'^api/benchmark-query/index/$', api_views.index_all_benchmarks),
    url(r'^api/benchmark-query/(?P<slug>[0-9a-z-]+)/$', api_views.PredifinedSearchApiView.as_view()),
    url(r'^api/benchmark-query/$', api_views.BenchmarkSearchApiViews.as_view()),

    url(r'^api/benchmark-query/$', api_views.BenchmarkSearchApiViews.as_view()),
]
