import datetime
import uuid

import argon2
from django.contrib.auth.decorators import user_passes_test
from django.http import HttpResponse, JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page

from elasticsearch import NotFoundError
from rest_framework.authentication import TokenAuthentication

from web.models import PredefinedSearch
from web.search import BenchmarkIndex
from rest_framework import status, mixins, generics
from rest_framework.response import Response
from web.models import Benchmark
from web.serializers import BenchmarkSerializer
from opendata import settings
from django.conf import settings

from opendata.settings import QUERY_CACHE_TTL, DOWNLOAD_CACHE_TTL

from rest_framework.permissions import IsAuthenticated


class BenchmarkListApiViews(generics.GenericAPIView):
    queryset = Benchmark.objects.all()
    serializer_class = BenchmarkSerializer

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        manage_id = uuid.uuid4()
        hashed_manage_id = self.hash_manage_id(manage_id)

        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            serializer.save(manage_id=hashed_manage_id)

            data = serializer.data;
            data['manage_id'] = manage_id

            return Response(data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @staticmethod
    def hash_manage_id(manage_id):
        return argon2.argon2_hash(str(manage_id), settings.MANAGE_ID_SALT, buflen=64)


class BenchmarkDetailApiViews(
    mixins.DestroyModelMixin,
    mixins.RetrieveModelMixin,
    generics.GenericAPIView):
    queryset = Benchmark.objects.all()
    serializer_class = BenchmarkSerializer

    def get(self, request, pk=None):
        try:
            benchmark = BenchmarkIndex.get(pk)
        except NotFoundError:
            return Response(status=status.HTTP_404_NOT_FOUND)

        data = benchmark.to_dict()
        data['benchmark_id'] = pk
        return Response(data);

    def delete(self, request, pk=None):
        manage_id = BenchmarkListApiViews.hash_manage_id(pk)

        try:
            benchmark = Benchmark.objects.get(manage_id=manage_id)
        except Benchmark.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        benchmark.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)


class BenchmarkSearchApiViews(mixins.ListModelMixin,
                              generics.GenericAPIView):
    queryset = Benchmark.objects.all()
    serializer_class = BenchmarkSerializer

    def get(self, request, *args, **kwargs):
        results = BenchmarkIndex.get_all()
        benchmarks = list()

        for benchmark in results.hits:
            benchmarks.append(benchmark.to_dict())

        print(benchmarks)

        return Response(benchmarks)


class PredifinedSearchApiView(mixins.RetrieveModelMixin,
                              generics.GenericAPIView):
    queryset = Benchmark.objects.all()
    serializer_class = BenchmarkSerializer

    @method_decorator(cache_page(QUERY_CACHE_TTL))
    def get(self, request, slug=None):

        try:
            pquery = PredefinedSearch.get_by_slug(slug)
        except PredefinedSearch.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        benchmarks, aggregations, hits = BenchmarkIndex.query(pquery.query)

        return Response({
            "hits": hits,
            "benchmarks": benchmarks,
            "aggregations": aggregations
        });


@user_passes_test(lambda u: u.is_superuser)
def index_all_benchmarks(request):
    BenchmarkIndex.bulk_indexing()
    return HttpResponse(status=200)


@cache_page(DOWNLOAD_CACHE_TTL)
def download_benchmarks(request):
    benchmarks = Benchmark.objects.all()
    dictionaries = [obj.as_dict() for obj in benchmarks]

    time = datetime.datetime.utcnow()

    data = {
        "meta": {
            "description": settings.DOWNLOAD_INFO,
            "date_created": time,
            "count": len(dictionaries),
            "license": "GPLv2",
        },
        "benchmarks": dictionaries
    }

    filename = 'benchmark-' + str(time) + '.json'
    response = JsonResponse(data, status=200)
    response['Content-Disposition'] = 'attachment; filename=' + filename
    return response
