from .models import Benchmark
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver


@receiver(post_save, sender=Benchmark)
def index_benchmark(sender, instance, **kwargs):
    instance.create_index()


@receiver(post_delete, sender=Benchmark)
def unindex_benchmark(sender, instance, **kwargs):
    instance.remove_index()
