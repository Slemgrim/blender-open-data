import os

from rest_framework.utils import json

root = os.path.realpath(os.path.dirname(__file__))
benchmark_schema = json.load(open(os.path.join(root, "../schemas", "benchmark.json")))
