from web import schema
from web.models import Benchmark
from rest_framework import serializers
from jsonschema import validate, ValidationError, SchemaError


class BenchmarkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Benchmark
        fields = ("benchmark_id", "date_created", "data")

    def create(self, validated_data):
        benchmark = Benchmark.objects.create(**validated_data)
        return benchmark

    def validate_data(self, value):

        try:
            validate(value, schema.benchmark_schema)
        except ValidationError as err:
            raise serializers.ValidationError(err.message)
        except SchemaError:
            raise serializers.ValidationError('Schema Invalid')
        return value
