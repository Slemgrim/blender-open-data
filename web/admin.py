from django.contrib import admin

from web.models import Benchmark, PredefinedSearch

admin.site.register(Benchmark)
admin.site.register(PredefinedSearch)
