from django.conf import settings
from elasticsearch import Elasticsearch
from elasticsearch.helpers import bulk
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import DocType, Text, Date, Object

from web import models

connections.create_connection(hosts=settings.ELASTIC_HOSTS)


class BenchmarkIndex(DocType):
    date_created = Date()
    benchmark_id = Text()
    data = Object()

    class Index:
        name = 'benchmark-test' if settings.TESTING else 'benchmark'

    @staticmethod
    def bulk_indexing():
        BenchmarkIndex.init()
        client = Elasticsearch()
        bulk(client=client, actions=(b.create_index() for b in models.Benchmark.objects.all().iterator()))

    @staticmethod
    def get_all():
        s = BenchmarkIndex.search()
        response = s.execute()
        return response

    @staticmethod
    def query(query, fields={}):
        s = super(BenchmarkIndex, BenchmarkIndex).search().from_dict(query)
        results = s.execute()

        benchmarks = list()
        for benchmark in results.hits:
            benchmarks.append(benchmark.to_dict())

        aggregations = results.aggregations.to_dict()

        return benchmarks, aggregations, results.hits.total
