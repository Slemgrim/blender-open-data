import json
import requests
import os

dirname = os.path.dirname(__file__)
base_url = "http://opendata.local:8002/"

headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Authorization': 'Token b4e40a77702d3fb563a17f213a19bdb60c7648a6'
}

# Create new benchmark

with open(os.path.join(dirname, 'benchmark.json')) as f:
    data = json.load(f)
    post_uri = base_url + 'benchmarks/'

    response = requests.post(post_uri, json=data, headers=headers)
    benchmark = json.loads(response.text)

print(response.url)
benchmark_id = benchmark['benchmark_id']
manage_id = benchmark['manage_id']

# Get benchmark

get_uri = base_url + "benchmarks/%s" % (benchmark_id)

response = requests.get(get_uri, headers=headers)
benchmark = response.json()
print(response.url)
print(benchmark)

# Get benchmarks from predifined elasticsearch query
get_uri = base_url + "benchmark-query/render-time-by-gpu/"

response = requests.get(get_uri, headers=headers)
benchmark = response.json()
print(response.url)
print(benchmark)

# Delete benchmark

delete_uri = base_url + "benchmarks/%s" % (manage_id)
response = requests.delete(delete_uri, headers=headers)
print(response.url)
print(response.status_code)

# Get deleted benchmark (404)

get_uri = base_url + "benchmarks/%s" % (benchmark_id)

response = requests.get(get_uri, headers=headers)
print(response.url)
print(response.status_code)
