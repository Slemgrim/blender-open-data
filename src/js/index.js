import 'bootstrap';

import '../scss/index.scss';
import pieChart from './diagrams/pie-chart.js';
import barChart from './diagrams/bar-chart.js';

window.onload = function (){
    pieChart(
        '#device-types',
        "/api/benchmark-query/device-type-count/",
        res => res.aggregations.devices.buckets.map(item => ({
            key: item.key,
            value: item.doc_count
        }))
    );


    barChart(
        '#render-time',
        "/api/benchmark-query/time-memory-percentiles/",
        res => {
            var data = [];

            for (var k in res.aggregations.average_render_time.values) {
                if (res.aggregations.average_render_time.values.hasOwnProperty(k)) {
                    data.push({
                        value: k,
                        key: res.aggregations.average_render_time.values[k]
                    });
                }
            }

            return data;
        }
    );
}

