import * as d3 from "d3";

export default function(selector, url, mutate){
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.setRequestHeader("Accept", "application/json");
    xhr.responseType = 'json';
    xhr.send();

    var svg = d3.select(selector),
        width = +svg.attr("width"),
        height = +svg.attr("height"),
        radius = Math.min(width, height) / 2,
        g = svg.append("g").attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

    var color = d3.scaleOrdinal(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);

    var pie = d3.pie()
        .value(function(d) { return +d.value; });

    var path = d3.arc()
        .outerRadius(radius - 10)
        .innerRadius(0);

    var label = d3.arc()
        .outerRadius(radius - 40)
        .innerRadius(radius - 100);


    xhr.addEventListener("load", function(evt){
        if(xhr.status != 200) {
            console.log("Request to " + url + "responded with: " + xhr.status);
            return;
        }

        var data;
        if(mutate && typeof mutate === "function") {
            data = mutate(evt.target.response)
        } else {
            data = evt.target.response;
        }

        var arc = g.selectAll(".pie-chart__arc")
        .data(pie(data))
        .enter().append("g")
          .attr("class", "pie-chart__arc");

        arc.append("path")
          .attr("d", path)
          .attr("fill", function(d) { return color(d.data.key); });

        arc.append("text")
          .attr("transform", function(d) { return "translate(" + label.centroid(d) + ")"; })
          .text(function(d) { return d.data.key; });
    });
}
