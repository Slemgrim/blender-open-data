import * as d3 from "d3";

export default function(selector, url, mutate){
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.setRequestHeader("Accept", "application/json");
    xhr.responseType = 'json';
    xhr.send();

    var svg = d3.select(selector),
    width = +svg.attr("width"),
    height = +svg.attr("height");


    var chart = svg.append("g");

    xhr.addEventListener("load", function(evt){
        if(xhr.status != 200) {
            console.log("Request to " + url + "responded with: " + xhr.status);
            return;
        }

        var data;
        if(mutate && typeof mutate === "function") {
            data = mutate(evt.target.response)
        } else {
            data = evt.target.response;
        }

        var keys = data.map(function(t) {
          return t.value
        });

        var xScale = d3.scaleBand()
          .domain(keys)
          .range([0, width])
          .paddingInner(0.1);

        var yScale = d3.scaleLinear()
          .domain([0, d3.max(data, function(d) { return d.key; })])
          .range([height, 0])
          .nice();

        var bandwidth = xScale.bandwidth();

        chart.selectAll('rect.bar')
          .data(data)
          .enter().append('rect')
            .classed('bar', true)
            .attr('x', function(d, i) {
              return xScale(d.value)
            })
            .attr('width', bandwidth)
            .attr('y', function(d) {
              return yScale(d.key);
            })
            .attr('height', function(d) {
              return height - yScale(d.key);
        });

        var xAxis = d3.axisBottom(xScale)
        .tickSizeOuter(0)
        .tickFormat(function(d) {
            return 'P' + Math.abs(d);
        });

        var yAxis = d3.axisLeft(yScale)
        .tickFormat(function(d) {
            return d + 's';
        });
        svg.append('g')
          .classed('bar-chart__label-x', true)
          .attr('transform', 'translate(0,' + (height - 30)+ ')')
          .call(xAxis);

        /* Disabled Y-Axis. Style this first!
        svg.append('g')
          .classed('bar-chart__label-y', true)
          .call(yAxis);
        */
    });
}
